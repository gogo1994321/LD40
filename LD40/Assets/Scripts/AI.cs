﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
public class AI : MonoBehaviour
{
    GameObject player;
	
    enum AIState
    {
        Circling,
        Attacking
    }

    AIState state = AIState.Circling;

    Vector3 targetPosition;

    [SerializeField]
    float circlingRadius = 5f;
    [SerializeField]
    float waitBetweenPositions = 2f;
    [SerializeField]
    int amountOfAttacks = 1;
    [SerializeField]
    float waitBetweenAttacks = 1f;
    [SerializeField]
    bool mayBlock = true;

    float currentTimer;
    int attacksLeft;
    float currentWaitBetweenAttacks;
    private AnimationController animationController;
    bool alive = true;
    Rigidbody _rigidB;
    float speed = 5f;


    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        targetPosition = transform.position;
        attacksLeft = amountOfAttacks;
        currentWaitBetweenAttacks = waitBetweenAttacks;
        animationController = GetComponent<AnimationController>();
        GetComponent<Health>().deathEvent.AddListener(() => Death());
        _rigidB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update ()
    {
        if (alive)
        {
            switch (state)
            {
                case AIState.Circling:
                    Circling();
                    animationController.SetRootMotion(false);
                    break;
                case AIState.Attacking:
                    Attacking();
                    animationController.SetRootMotion(true);
                    break;
            }
            Vector3 targetDir = player.transform.position - transform.position;
            targetDir.y = 0;
            transform.rotation = Quaternion.LookRotation(targetDir);
            animationController.moving = _rigidB.velocity.magnitude > 0;

        }
    }

    void Circling()
    {
        if (currentTimer > 0 && Vector3.Distance(transform.position, targetPosition) > 1f)
        {
            currentTimer -= Time.deltaTime;
            Vector3 dir = targetPosition - transform.position;
            _rigidB.velocity = dir.normalized * speed;
        }
        else
        {
            bool isSolidGround = false;
            int maxTries = 1000;
            while (!isSolidGround && maxTries > 0)
            {
                float randomAngle = Random.Range(0, 360);
                targetPosition.x = player.transform.position.x + circlingRadius * Mathf.Cos(randomAngle);
                targetPosition.z = player.transform.position.z + circlingRadius * Mathf.Sin(randomAngle);
                targetPosition.y = transform.position.y;
                isSolidGround = Physics.Raycast(targetPosition+new Vector3(0,1,0),-Vector3.up);
                maxTries--;
            }
            currentTimer = waitBetweenPositions;
            _rigidB.velocity = Vector3.zero;
        }
    }

    void Attacking()
    {
        targetPosition = player.transform.position;
        if (Vector3.Distance(player.transform.position, transform.position) > 5f)
        {
            Vector3 dir = targetPosition - transform.position;
            _rigidB.velocity = dir.normalized * speed;
            return;
        }else if (attacksLeft > 0 && currentWaitBetweenAttacks <= 0)
        {
            attacksLeft--;
            if (Random.value + System.Convert.ToInt32(animationController.blocking) < 0.5 || !mayBlock)
            {
                currentWaitBetweenAttacks = waitBetweenAttacks;
                animationController.DoAttack();
            }
            else
            {
                currentWaitBetweenAttacks = waitBetweenAttacks / 2;
                animationController.DoBlock(!animationController.blocking);
            }
        }
        else if (currentWaitBetweenAttacks > 0 && attacksLeft > 0)
        {
            currentWaitBetweenAttacks -= Time.deltaTime;
        }
        else
        {
            state = AIState.Circling;
            attacksLeft = amountOfAttacks;
        }
        _rigidB.velocity = Vector3.zero;
    }

    public void SwitchState(int stateIndex)
    {
        state = (AIState)stateIndex;
        Debug.Log(state);
    }

    public int GetStateIndex()
    {
        return (int)state;
    }

    void Death()
    {
        alive = false;
    }
}
