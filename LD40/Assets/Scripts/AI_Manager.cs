﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Wave
{
    public GameObject[] enemyPrefabs;
    public int count;
    public Transform[] spawnPoints;
    public UnityEvent waveFinished;

}

public class AI_Manager : MonoBehaviour
{
    List<AI> AIs = new List<AI>();

    [SerializeField]
    int maxAIAttackersAtOnce = 3;

    [SerializeField]
    Vector2 attackTimerRange;

    float currentTimer;

    List<AI> currentAttackers = new List<AI>();


    public List<Wave> waves = new List<Wave>();

    int currentWave;

	void Update ()
    {
        if (AIs.Count > 0)
        {
            if (AllAttackersFinished())
            {
                if (currentTimer > 0)
                {
                    currentTimer -= Time.deltaTime;
                }
                else
                {
                    currentAttackers.Clear();
                    for (int i = 0; i < Random.Range(1, maxAIAttackersAtOnce + 1); i++)
                    {
                        int index = Random.Range(0, AIs.Count);
                        AIs[index].SwitchState(1);
                        currentAttackers.Add(AIs[index]);
                    }
                    currentTimer = Random.Range(attackTimerRange.x, attackTimerRange.y);
                }
            }
        }
        else if(currentWave < waves.Count)
        {
           
            Wave wave = waves[currentWave];
            wave.waveFinished.Invoke();
            for (int i = 0; i < wave.count; i++)
            {
                AI ai = Instantiate(wave.enemyPrefabs[Random.Range(0, wave.enemyPrefabs.Length)], wave.spawnPoints[Random.Range(0, wave.spawnPoints.Length)].position, Quaternion.identity).GetComponent<AI>();
                ai.GetComponent<Health>().deathEvent.AddListener(() => AIs.Remove(ai));
                AIs.Add(ai);
            }
            if (AIs.Count <= 0 && currentWave == waves.Count-1)
                FindObjectOfType<GameManager>().WinGame();

            currentWave++;
        }
	}

    bool AllAttackersFinished()
    {
        bool finished = true;
        foreach (AI ai in currentAttackers)
        {
            if (ai != null && ai.GetStateIndex() == 1)
                finished = false;
        }

        return finished;
    }
}
