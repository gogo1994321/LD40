﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{


    Animator animator;
    [HideInInspector]
    public bool moving;
    public bool blocking;
    public Damager weapon;

    [SerializeField]
    AudioClip[] swordSwish;

	// Use this for initialization
	void Start ()
    {
        animator = GetComponentInChildren<Animator>();
        GetComponent<Health>().deathEvent.AddListener(() => Death());
	}
	
	// Update is called once per frame
	void Update ()
    {
        animator.SetBool("moving", moving);
	}

    public void DoAttack()
    {
        animator.SetInteger("attackIndex", Random.Range(0, 5));
        animator.SetTrigger("attack");

    }

    public void DoDash()
    {
        animator.SetTrigger("dash");
    }

    public void AttackStart()
    {
        GetComponent<AudioSource>().PlayOneShot(swordSwish[Random.Range(0,swordSwish.Length)]);
        weapon.canDamage = true;
    }

    public void AttackEnd()
    {
        weapon.canDamage = false;
    }

    public void Death()
    {
        animator.SetInteger("deathIndex", Random.Range(0, 3));
        animator.SetTrigger("death");
    }

    public void DoBlock(bool blocking)
    {
        this.blocking = blocking;
        animator.SetBool("blocking", blocking);
    }

    public void SetRootMotion(bool root)
    {
        animator.applyRootMotion = root;
    }
}
