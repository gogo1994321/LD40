﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject objectToFollow;

    [SerializeField]
    float cameraRotateSpeed = 10f;

    [SerializeField]
    float distanceFromObject = 5f;

    [SerializeField]
    float followSpeed = 1f;


    float followP;
    [SerializeField]
    float heightToKeep;
    Vector3 lerpStartPos;

    float currentDistanceFromObject;
    float minDistanceFromObject;

    void Start()
    {
        currentDistanceFromObject = distanceFromObject;
        minDistanceFromObject = distanceFromObject / 4;
    }

    void Update()
    {
        

        Vector3 dirFromObject = (transform.position - objectToFollow.transform.position);
        RaycastHit hit;

        Vector3 wantedPos = objectToFollow.transform.position + dirFromObject.normalized * currentDistanceFromObject;

        if (Physics.Raycast(transform.position, dirFromObject * -1,out hit, distanceFromObject) && hit.transform.gameObject != objectToFollow && hit.transform.gameObject.isStatic)
        {
            wantedPos.x = hit.point.x;
            wantedPos.z = hit.point.z;
        }

        wantedPos.y = objectToFollow.transform.position.y + heightToKeep;

        transform.position = Vector3.Lerp(transform.position, wantedPos, followSpeed*Time.deltaTime);

        Quaternion rot = Quaternion.LookRotation(dirFromObject * -1);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * cameraRotateSpeed);
        
        transform.RotateAround(objectToFollow.transform.position, Vector3.up, Input.GetAxis("Mouse X") * cameraRotateSpeed * Time.deltaTime);
    }
}
