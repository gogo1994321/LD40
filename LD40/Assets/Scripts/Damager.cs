﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    [SerializeField]
    float defaultDamage = 10f;

    [SerializeField]
    float damageIncrease = 5f;

    float currentDamage;

    public bool canDamage;

    void Start()
    {
        currentDamage = defaultDamage;
    }

	void OnTriggerEnter (Collider collider)
    {
        if (collider.GetComponent<Health>() != null && collider.transform != transform.root && canDamage)
        {
            Debug.Log(collider.name+"!="+transform.root);
            collider.GetComponent<Health>().DoDamage(currentDamage);
        }
	}

    public void ModifyDamage(float percentage)
    {
        currentDamage = defaultDamage + defaultDamage * percentage*damageIncrease;
    }

    public float GetDamage()
    {
        return currentDamage;
    }
}
