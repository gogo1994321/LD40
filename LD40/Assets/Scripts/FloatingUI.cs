﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingUI : MonoBehaviour
{

    public Image healthBar;
    public Text healthText;
    public Image damageBar;
    public Text damageText;
}
