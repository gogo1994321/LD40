﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    [SerializeField]
    float waitBeforeDeathScreen = 3f;

	// Use this for initialization
	void Start ()
    {
        if(GameObject.FindGameObjectWithTag("Player") != null)
            GameObject.FindGameObjectWithTag("Player").GetComponent<Health>().deathEvent.AddListener(() => StartCoroutine(PlayerDead()));
    }
	
    public void LoadMainLevel()
    {
        SceneManager.LoadScene("DEV");
    }

	IEnumerator PlayerDead()
    {
        float timer = waitBeforeDeathScreen;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        SceneManager.LoadSceneAsync("Death");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void WinGame()
    {
        SceneManager.LoadScene("Win");
    }
}
