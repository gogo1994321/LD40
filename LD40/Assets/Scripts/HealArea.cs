﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealArea : MonoBehaviour
{
    public float healingSpeed = 1f;
	void OnTriggerStay(Collider collider)
    {
        if(collider.GetComponent<Health>() != null)
        {
            collider.GetComponent<Health>().DoDamage(-Time.deltaTime * healingSpeed);
        }
    }
}
