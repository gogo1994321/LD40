﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    float currentHealth;

    [SerializeField]
    private float maxHealth;

    [SerializeField]
    Vector3 uiOffset;

    [SerializeField]
    GameObject healthUI;

    [SerializeField]
    bool spawnUI = true;

    [SerializeField]
    bool modifyDamage;

    [SerializeField]
    Damager weapon;

    GameObject canvas;
    [HideInInspector]
    public UnityEvent deathEvent;
    bool dead;

    [SerializeField]
    AudioClip deathSound;
    [SerializeField]
    AudioClip hurtSound;
    [SerializeField]
    AudioClip shieldSound;
    // Use this for initialization
    void Start () {
        currentHealth = maxHealth;
        canvas = GameObject.FindGameObjectWithTag("MainCanvas");
        if (spawnUI)
        {
            healthUI = Instantiate(healthUI);
            healthUI.transform.SetParent(canvas.transform);
        }
	}

    public void DoDamage(float damage)
    {
        if (!dead)
        {
            if (!GetComponent<AnimationController>().blocking)
            {
                if (currentHealth - damage > 0)
                {
                    currentHealth -= damage;
                    if (currentHealth > maxHealth)
                        currentHealth = maxHealth;

                    if (hurtSound != null && damage > 0)
                        GetComponent<AudioSource>().PlayOneShot(hurtSound);
                    if (modifyDamage)
                        weapon.ModifyDamage(1.0f - (currentHealth / maxHealth));
                }
                else
                {
                    currentHealth = 0;
                    if (deathSound != null)
                        GetComponent<AudioSource>().PlayOneShot(deathSound);

                    deathEvent.Invoke();
                    dead = true;
                    UpdateUI();
                    StartCoroutine(Dead());
                }
            }
            else
            {
                GetComponent<Rigidbody>().AddForce(-transform.forward.normalized * 50, ForceMode.VelocityChange);
                GetComponent<AudioSource>().PlayOneShot(shieldSound);
            }
        }
    }

    void LateUpdate()
    {
        if (spawnUI)
        {
            healthUI.transform.position = transform.position + transform.TransformVector(uiOffset);
            healthUI.transform.LookAt(Camera.main.transform);
        }
        UpdateUI();
    }

    void UpdateUI()
    {
        FloatingUI floatingUI = healthUI.GetComponent<FloatingUI>();
        floatingUI.healthText.text = Mathf.Round(currentHealth) + "/" + maxHealth;
        floatingUI.healthBar.fillAmount = currentHealth / maxHealth;
        floatingUI.damageBar.fillAmount = modifyDamage ? 1.0f - (currentHealth / maxHealth) : 1f;
        floatingUI.damageText.text = weapon.GetDamage().ToString();
    }

    IEnumerator Dead()
    {
        float timer = 2f;
        while(timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        Destroy(gameObject);
    }
}
