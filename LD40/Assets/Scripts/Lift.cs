﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : MonoBehaviour
{
    [SerializeField]
    Vector3 targetPos;

    [SerializeField]
    float speed;

    Vector3 startPos;

    float progress = 1f;

    void Start()
    {
        startPos = transform.localPosition;
    }
	
	public void Update ()
    {
        if(progress < 1)
        {
            transform.localPosition = Vector3.Lerp(startPos, targetPos, progress);
            progress += Time.deltaTime * speed;
        }
	}

    public void Move()
    {
        progress = 0;
    }
}
