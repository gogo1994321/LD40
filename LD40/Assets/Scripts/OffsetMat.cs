﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetMat : MonoBehaviour
{
    Material mat;
    public float speed = 1f;
    float p = 0;

    void Start()
    {
        mat = GetComponent<Renderer>().material;
    }

	void Update ()
    {
        mat.SetTextureOffset("_MainTex",new Vector2(0,p));
        p = (p + Time.deltaTime*speed) % 1f;
	}
}
