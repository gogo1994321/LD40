﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    public float Speed = 5f;
    public float JumpHeight = 2f;
    public float GroundDistance = 0.2f;
    public float DashDistance = 5f;
    public LayerMask Ground;
    public GameObject smokePuff;
    public GameObject trail;


    private Vector3 _inputs = Vector3.zero;
    private bool _isGrounded = true;
    private AnimationController animationController;
    private bool canDodge = true;

    void Start()
    {

        animationController = GetComponent<AnimationController>();
    }

    void Update()
    {
        _isGrounded = Physics.Raycast(transform.position, -Vector3.up, GroundDistance, Ground, QueryTriggerInteraction.Ignore);

        Vector3 dirFromCamera = (transform.position - Camera.main.transform.position).normalized;
        dirFromCamera.y = 0;

        
        _inputs = (Input.GetAxis("Horizontal") * Speed * Camera.main.transform.right)+(Input.GetAxis("Vertical")*Speed*dirFromCamera);
        if (_inputs != Vector3.zero)
            transform.rotation = Quaternion.LookRotation(_inputs);

        animationController.moving = (_inputs != Vector3.zero);

        if (Input.GetButtonDown("Dodge") && canDodge)
        {
            StartCoroutine("Dash");
        }
        if (Input.GetButtonDown("Attack"))
        {
            animationController.DoAttack();
        }
        if (Input.GetButtonDown("Block"))
        {
            animationController.DoBlock(true);
        }
        if (Input.GetButtonUp("Block"))
        {
            animationController.DoBlock(false);
        }
    }

    IEnumerator Dash()
    {
        animationController.DoDash();
        float progress = 0;
        Vector3 startPos = transform.position;
        Vector3 forward = transform.forward.normalized;
        Vector3 endPos = startPos + forward * DashDistance;
        canDodge = false;
        Instantiate(smokePuff, transform.position, Quaternion.identity);
        trail.SetActive(true);
        RaycastHit hit;
        if (Physics.Raycast(transform.position+Vector3.up, forward, out hit, DashDistance))
            endPos = hit.point;

        while(progress < 1f)
        {
            transform.position = Vector3.Lerp(startPos, endPos, progress);
            progress += Time.deltaTime*2;
            yield return null;
        }
        if (progress >= 1f)
        {
            canDodge = true;
            trail.SetActive(false);
        }
    }
}