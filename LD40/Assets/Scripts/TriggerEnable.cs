﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnable : MonoBehaviour {

    [SerializeField]
    GameObject GO;

	void OnTriggerEnter()
    {
        GO.SetActive(true);
    }
}
