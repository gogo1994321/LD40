﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryMove : MonoBehaviour {


	public GameObject MyCamera;
	private Vector3 startPos;
	private Vector3 endPos;
	private float distance = 30f;

	public float lerpTime = 5;
	private float currentLerpTime = 0;


	// Use this for initialization
	void Start () {
		startPos = MyCamera.transform.position;
		endPos = MyCamera.transform.position + Vector3.forward * distance;
	}
	
	// Update is called once per frame
	void Update () {
		currentLerpTime += Time.deltaTime;
		if (currentLerpTime >= lerpTime) {
			currentLerpTime = lerpTime;
		}
		float Perc = currentLerpTime / lerpTime;
		MyCamera.transform.position = Vector3.Lerp (startPos, endPos, Perc);
	
	}

}
