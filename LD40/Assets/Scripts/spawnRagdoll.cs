﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnRagdoll : MonoBehaviour
{
    public GameObject ragdoll;

    void Start()
    {
        GetComponent<Health>().deathEvent.AddListener(() => DoRagdoll());
    }
	
    void DoRagdoll()
    {
        ragdoll = Instantiate(ragdoll, transform.position, transform.rotation);
        Rigidbody[] rigidbodies_original = transform.GetComponentsInChildren<Rigidbody>();
        Rigidbody[] rigidbodies = ragdoll.transform.GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody rig in rigidbodies_original)
        {
            foreach (Rigidbody rig2 in rigidbodies)
            {
                if (rig.name == rig2.name)
                {
                    rig2.transform.position = rig.transform.position;
                    rig2.transform.rotation = rig.transform.rotation;
                }
            }
        }

        gameObject.SetActive(false);
    }
}
